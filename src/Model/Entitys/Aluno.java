package Model.Entitys;

public class Aluno {
   private int codE;
   private String nome;
   private String apelido;
   private String dataNasc;
   private String conctato;
   private String morada;
   private String anoEntrada;
   private String situacao;
   private String nomeFacul;
   private String classesFeitas;
   private int notas; 
   private int codM;

    public int getCodM() {
        return codM;
    }

    public void setCodM(int codM) {
        this.codM = codM;
    }
   private int codC;

    public int getCodC() {
        return codC;
    }

    public void setCodC(int codC) {
        this.codC = codC;
    }
    public int getCodE() {
        return codE;
    }

    public void setCodE(int codE) {
        this.codE = codE;
    }
 

    public int getNotas() {
        return notas;
    }

    public void setNotas(int notas) {
        this.notas = notas;
    }
    public String getClassesFeitas() {
        return classesFeitas;
    }

    public void setClassesFeitas(String classesFeitas) {
        this.classesFeitas = classesFeitas;
    }
    public String getNomeFacul() {
        return nomeFacul;
    }

    public void setNomeFacul(String nomeFacul) {
        this.nomeFacul = nomeFacul;
    }
   private int idEstudante;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public String getDataNasc() {
        return dataNasc;
    }

    public void setDataNasc(String dataNasc) {
        this.dataNasc = dataNasc;
    }

    public String getConctato() {
        return conctato;
    }

    public void setConctato(String conctato) {
        this.conctato = conctato;
    }

    public String getMorada() {
        return morada;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }

    public String getAnoEntrada() {
        return anoEntrada;
    }

    public void setAnoEntrada(String anoEntrada) {
        this.anoEntrada = anoEntrada;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao =situacao;
    }

    public int getIdEstudante() {
        return idEstudante;
    }

    public void setIdEstudante(int idEstudante) {
        this.idEstudante = idEstudante;
    }
   
}
