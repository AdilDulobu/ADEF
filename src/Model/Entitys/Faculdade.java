package Model.Entitys;

public class Faculdade {
         private int idEstudFaculade;
        private String nomeFaculdade;
        private String localizacao;
        private String observacoes;

    public int getIdEstudFaculade() {
        return idEstudFaculade;
    }

    public void setIdEstudFaculade(int idEstudFaculade) {
        this.idEstudFaculade = idEstudFaculade;
    }

    public String getNomeFaculdade() {
        return nomeFaculdade;
    }

    public void setNomeFaculdade(String nomeFaculdade) {
        this.nomeFaculdade = nomeFaculdade;
    }

    public String getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }
        
         
}
