
package Model.Entitys;


public class TrabalhoAdministrativo {
        private int estudateTrabAdmin;
        private String trabalhosRealizados;
        private double avaliacaoTrab;

    public int getEstudateTrabAdmin() {
        return estudateTrabAdmin;
    }

    public void setEstudateTrabAdmin(int estudateTrabAdmin) {
        this.estudateTrabAdmin = estudateTrabAdmin;
    }

    public String getTrabalhosRealizados() {
        return trabalhosRealizados;
    }

    public void setTrabalhosRealizados(String trabalhosRealizados) {
        this.trabalhosRealizados = trabalhosRealizados;
    }

    public double getAvaliacaoTrab() {
        return avaliacaoTrab;
    }

    public void setAvaliacaoTrab(double avaliacaoTrab) {
        this.avaliacaoTrab = avaliacaoTrab;
    }
        
}
