package Model.Entitys;

public class Curso {
    private int idEstudCurso;
    private String nomeCurso;
    private int duracao;
    private byte anoInicio;
    private byte anoConclusao;
    private String periodo;
    private byte anoActual;

    public int getIdEstudCurso() {
        return idEstudCurso;
    }

    public void setIdEstudCurso(int idEstudCurso) {
        this.idEstudCurso = idEstudCurso;
    }

    public String getNomeCurso() {
        return nomeCurso;
    }

    public void setNomeCurso(String nomeCurso) {
        this.nomeCurso = nomeCurso;
    }

    public int getDuracao() {
        return duracao;
    }

    public void setDuracao(int duracao) {
        this.duracao = duracao;
    }

    public byte getAnoInicio() {
        return anoInicio;
    }

    public void setAnoInicio(byte anoInicio) {
        this.anoInicio = anoInicio;
    }

    public byte getAnoConclusao() {
        return anoConclusao;
    }

    public void setAnoConclusao(byte anoConclusao) {
        this.anoConclusao = anoConclusao;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public byte getAnoActual() {
        return anoActual;
    }

    public void setAnoActual(byte anoActual) {
        this.anoActual = anoActual;
    }
    
   
}
