package Model.Entitys;

public class Madrassa {
    private int estudanteMadressa;
    private byte presencas;
    private byte faltas;
    private String nivel;
    private byte avaliacao;

    public int getEstudanteMadressa() {
        return estudanteMadressa;
    }

    public void setEstudanteMadressa(int estudanteMadressa) {
        this.estudanteMadressa = estudanteMadressa;
    }

    public byte getPresencas() {
        return presencas;
    }

    public void setPresencas(byte presencas) {
        this.presencas = presencas;
    }

    public byte getFaltas() {
        return faltas;
    }

    public void setFaltas(byte faltas) {
        this.faltas = faltas;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public byte getAvaliacao() {
        return avaliacao;
    }

    public void setAvaliacao(byte avaliacao) {
        this.avaliacao = avaliacao;
    }
    
}
