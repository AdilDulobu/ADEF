package Model.Entitys;


public class Despesas {
     private int idEstudDesp;
    private double habitacao;
    private double propinas;
    private double saude;
    private double alimentacao;
    private double transporte;

    public int getIdEstudDesp() {
        return idEstudDesp;
    }

    public void setIdEstudDesp(int idEstudDesp) {
        this.idEstudDesp = idEstudDesp;
    }

    public double getHabitacao() {
        return habitacao;
    }

    public void setHabitacao(double habitacao) {
        this.habitacao = habitacao;
    }

    public double getPropinas() {
        return propinas;
    }

    public void setPropinas(double propinas) {
        this.propinas = propinas;
    }

    public double getSaude() {
        return saude;
    }

    public void setSaude(double saude) {
        this.saude = saude;
    }

    public double getAlimentacao() {
        return alimentacao;
    }

    public void setAlimentacao(double alimentacao) {
        this.alimentacao = alimentacao;
    }

    public double getTransporte() {
        return transporte;
    }

    public void setTransporte(double transporte) {
        this.transporte = transporte;
    }
    
}
