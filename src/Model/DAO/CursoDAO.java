
package Model.DAO;

import Model.Entitys.Curso;
import conexao.BDconexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class CursoDAO {
   private Connection conexao;
    
    public CursoDAO() {
         try{
            conexao=BDconexao.getConnection();
        }catch(SQLException|ClassNotFoundException ex){
            System.out.println("Erro de conexao: "+ex.getMessage());            
        }
    }
    public void inserir(Curso c){
         String query="INSERT INTO curso(codC,nomeCurso,regime,duracao) VALUES(?,?,?,?)";
             try {
                    PreparedStatement stmt=conexao.prepareStatement(query); 
                    stmt.setInt(1,c.getIdEstudCurso());
                    stmt.setString(2,c.getNomeCurso());
                    stmt.setString(3,c.getPeriodo());
                    stmt.setInt(4,c.getDuracao());
                        stmt.executeUpdate();
             }catch(SQLException ex){
                 System.out.println("Erro de insercao da base de dados: "+ex.getMessage());
             }
    }
    
}
