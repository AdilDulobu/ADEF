/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.DAO;

import Model.Entitys.Curso;
import Model.Entitys.Madrassa;
import conexao.BDconexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Elias
 */
public class MadrassaDAO {
      private Connection conexao;

    public MadrassaDAO() {
          try{
            conexao=BDconexao.getConnection();
        }catch(SQLException|ClassNotFoundException ex){
            System.out.println("Erro de conexao: "+ex.getMessage());            
        }
    }
    
     public void inserir(Madrassa m){
         String query="INSERT INTO madrassa(codM,nivel,presenca) VALUES(?,?,?)";
             try {
                    PreparedStatement stmt=conexao.prepareStatement(query); 
                    stmt.setInt(1,m.getEstudanteMadressa());
                    stmt.setString(2,m.getNivel());
                    stmt.setInt(3,m.getPresencas());
             }catch(SQLException ex){
                 System.out.println("Erro de insercao da base de dados: "+ex.getMessage());
             }
    }
    
    
    
    
    
}
