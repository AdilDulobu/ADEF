/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.DAO;

import Model.Entitys.Curso;
import Model.Entitys.Despesas;
import conexao.BDconexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Elias
 */
public class DespesasDAO {
      private Connection conexao;

    public DespesasDAO() {
             try{
            conexao=BDconexao.getConnection();
        }catch(SQLException|ClassNotFoundException ex){
            System.out.println("Erro de conexao: "+ex.getMessage());            
        }
    }
       public void inserir( Despesas d){
         String query="INSERT INTO gastos(codG,habitacao,alimentacao,transporte,saude,propina) VALUES(?,?,?,?,?,?)";
             try {
                  PreparedStatement stmt=conexao.prepareStatement(query); 
                 stmt.setInt(1,d.getIdEstudDesp());
                 stmt.setDouble(2,d.getHabitacao());
                 stmt.setDouble(3,d.getAlimentacao());
                 stmt.setDouble(4,d.getTransporte());
                 stmt.setDouble(5,d.getSaude());
                 stmt.setDouble(6,d.getPropinas());
                   }catch(SQLException ex){
                 System.out.println("Erro de insercao da base de dados: "+ex.getMessage());
             }
             }
}
