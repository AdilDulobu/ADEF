package Model.DAO;
import Model.Entitys.Aluno;
import conexao.BDconexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
public class AlunoDAO {
    
    private Connection conexao;
    
    public AlunoDAO(){
        
        try{
            conexao=BDconexao.getConnection();
        }catch(SQLException|ClassNotFoundException ex){
            System.out.println("Erro de conexao: "+ex.getMessage());            
        }
    }
    
    public void inserir(Aluno a){
        String query="INSERT INTO estudante_historico(codE,nome,apelido,dataNascimento,contacto,morada,anoEntradaAdef,situacao,nomeFaculdade,classesFeitas,notas,codMad,codCurso) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        
        try {
           PreparedStatement stmt=conexao.prepareStatement(query); 
            stmt.setInt(1,a.getCodE());
            stmt.setString(2,a.getNome());
            stmt.setString(3,a.getApelido());
            stmt.setString(4,a.getDataNasc());
            stmt.setString(5,a.getConctato());
            stmt.setString(6,a.getMorada());
            stmt.setString(7,a.getAnoEntrada());
            stmt.setString(8,a.getSituacao());
            stmt.setString(9,a.getNomeFacul());
            stmt.setString(10,a.getClassesFeitas());
            stmt.setInt(11,a.getNotas());
            stmt.setInt(12,a.getCodM());
            stmt.setInt(13,a.getCodC());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Erro de insercao da base de dados: "+ex.getMessage());
        }
        
    }
}    
