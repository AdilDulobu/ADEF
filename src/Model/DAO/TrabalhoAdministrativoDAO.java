/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.DAO;

import Model.Entitys.Curso;
import Model.Entitys.TrabalhoAdministrativo;
import conexao.BDconexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Elias
 */
public class TrabalhoAdministrativoDAO {
     private Connection conexao;

    public TrabalhoAdministrativoDAO() {
        try{
            conexao=BDconexao.getConnection();
        }catch(SQLException|ClassNotFoundException ex){
            System.out.println("Erro de conexao: "+ex.getMessage());            
        }
    }
    
      public void inserir(TrabalhoAdministrativo t){
         String query="INSERT INTO trabalhoadmin(codTrabAdm,descricao,avaliacao) VALUES(?,?,?)";
             try {
                    PreparedStatement stmt=conexao.prepareStatement(query); 
                    stmt.setInt(1,t.getEstudateTrabAdmin());
                    stmt.setString(2,t.getTrabalhosRealizados());
                    stmt.setDouble(3,t.getAvaliacaoTrab());
             }catch(SQLException ex){
                 System.out.println("Erro de insercao da base de dados: "+ex.getMessage());
             }
    }
}
